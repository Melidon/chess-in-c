#include <stdlib.h>

#include "board.h"
#include "piece.h"
#include "square.h"

static Square *Board_GetSquareAt(Board *this, char x, int y) {
    if (x < 'A' || x > 'H' || y < 1 || y > 8) {
        return NULL;
    }
    return &this->squares[x - 'A'][8 - y];
}

static void Board_Clear(Board *this) {
    for (char x = 'A'; x <= 'H'; ++x) {
        for (int y = 1; y <= 8; ++y) {
            Square *square = Board_GetSquareAt(this, x, y);
            Square_SetPiece(square, NULL);
        }
    }
}

static void Board_BasicSetup(Board *this) {
    // Just to make sure
    Board_Clear(this);
    // Black rooks
    Piece *black_rook_1 = Piece_New(BLACK, ROOK);
    Square *A8 = Board_GetSquareAt(this, 'A', 8);
    Square_SetPiece(A8, black_rook_1);
    Piece *black_rook_2 = Piece_New(BLACK, ROOK);
    Square *H8 = Board_GetSquareAt(this, 'H', 8);
    Square_SetPiece(H8, black_rook_2);
    // Black knights
    Piece *black_knight_1 = Piece_New(BLACK, KNIGHT);
    Square *B8 = Board_GetSquareAt(this, 'B', 8);
    Square_SetPiece(B8, black_knight_1);
    Piece *black_knight_2 = Piece_New(BLACK, KNIGHT);
    Square *G8 = Board_GetSquareAt(this, 'G', 8);
    Square_SetPiece(G8, black_knight_2);
    // Black bishops
    Piece *black_bishop_1 = Piece_New(BLACK, BISHOP);
    Square *C8 = Board_GetSquareAt(this, 'C', 8);
    Square_SetPiece(C8, black_bishop_1);
    Piece *black_bishop_2 = Piece_New(BLACK, BISHOP);
    Square *F8 = Board_GetSquareAt(this, 'F', 8);
    Square_SetPiece(F8, black_bishop_2);
    // Black queen
    Piece *black_queen = Piece_New(BLACK, QUEEN);
    Square *D8 = Board_GetSquareAt(this, 'D', 8);
    Square_SetPiece(D8, black_queen);
    // Black king
    Piece *black_king = Piece_New(BLACK, KING);
    Square *E8 = Board_GetSquareAt(this, 'E', 8);
    Square_SetPiece(E8, black_king);
    // Black pawns
    for (char i = 'A'; i <= 'H'; ++i) {
        Piece *black_pawn = Piece_New(BLACK, PAWN);
        Square *square = Board_GetSquareAt(this, i, 7);
        Square_SetPiece(square, black_pawn);
    }
    // White rooks
    Piece *white_rook_1 = Piece_New(WHITE, ROOK);
    Square *A1 = Board_GetSquareAt(this, 'A', 1);
    Square_SetPiece(A1, white_rook_1);
    Piece *white_rook_2 = Piece_New(WHITE, ROOK);
    Square *H1 = Board_GetSquareAt(this, 'H', 1);
    Square_SetPiece(H1, white_rook_2);
    // White knights
    Piece *white_knight_1 = Piece_New(WHITE, KNIGHT);
    Square *B1 = Board_GetSquareAt(this, 'B', 1);
    Square_SetPiece(B1, white_knight_1);
    Piece *white_knight_2 = Piece_New(WHITE, KNIGHT);
    Square *G1 = Board_GetSquareAt(this, 'G', 1);
    Square_SetPiece(G1, white_knight_2);
    // White bishops
    Piece *white_bishop_1 = Piece_New(WHITE, BISHOP);
    Square *C1 = Board_GetSquareAt(this, 'C', 1);
    Square_SetPiece(C1, white_bishop_1);
    Piece *white_bishop_2 = Piece_New(WHITE, BISHOP);
    Square *F1 = Board_GetSquareAt(this, 'F', 1);
    Square_SetPiece(F1, white_bishop_2);
    // White queen
    Piece *white_queen = Piece_New(WHITE, QUEEN);
    Square *D1 = Board_GetSquareAt(this, 'D', 1);
    Square_SetPiece(D1, white_queen);
    // White king
    Piece *white_king = Piece_New(WHITE, KING);
    Square *E1 = Board_GetSquareAt(this, 'E', 1);
    Square_SetPiece(E1, white_king);
    // White pawns
    for (char i = 'A'; i <= 'H'; ++i) {
        Piece *white_pawn = Piece_New(WHITE, PAWN);
        Square *square = Board_GetSquareAt(this, i, 2);
        Square_SetPiece(square, white_pawn);
    }
}

void Board_Construct(Board *this) {
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            Square_Construct(&this->squares[i][j], i, j);
        }
    }
    // Basic setup
    Board_BasicSetup(this);
}

void Board_Destruct(Board *this) {
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            Square_Destruct(&this->squares[i][j]);
        }
    }
}
