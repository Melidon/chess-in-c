#include <stdlib.h>

#include "position.h"
#include "square.h"

Color Square_GetColor(const Square *this) {
    if ((this->position.x + this->position.y) % 2 == 0) {
        return WHITE;
    } else {
        return BLACK;
    }
}

void Square_SetPiece(Square *this, Piece *piece) {
    if (this->piece != NULL) {
        Piece_Delete(this->piece);
    }
    this->piece = piece;
}

void Square_Construct(Square *this, int x, int y) {
    *this = (Square){.position = (Position){.x = x, .y = y}, .piece = NULL};
}

void Square_Destruct(Square *this) {
    if (this->piece != NULL) {
        Piece_Delete(this->piece);
    }
}
