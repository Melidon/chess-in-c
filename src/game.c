#include <stdlib.h>

#include "game.h"
#include "position.h"
#include "square.h"

void Game_Construct(Game *this, int width, int height) {
    Board_Construct(&this->board);
    // Initializing SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot initialize SDL: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    // Creating window
    this->window = SDL_CreateWindow("Chess", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0);
    if (this->window == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot create window: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    // Creating renderer
    this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
    if (this->renderer == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot create renderer: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    // Clearing the renderer
    if (SDL_RenderClear(this->renderer) < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot clear renderer: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    if (IMG_Init(IMG_INIT_PNG) < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot initialize SDL_image: %s", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    // Loading textures
    this->pieces_texture = IMG_LoadTexture(this->renderer, "assets/pieces.png");
    if (this->pieces_texture == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot load texture: %s", IMG_GetError());
        exit(EXIT_FAILURE);
    }
    // Calculating square_size
    this->square_size = (width <= height ? width : height) / 9;
    // Setting edge_size
    this->edge_size = this->square_size / 2;
    // Loading font
    if (TTF_Init() < 0) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot initialize SDL_ttf: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    this->font = TTF_OpenFont("assets/CalibriL.ttf", this->square_size);
    if (this->font == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Cannot load font: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    // Calculating board_offset
    this->board_offset = (Position){.x = (width <= height ? 0 : (width - 9 * this->square_size) / 2),
                                    .y = (width <= height ? (height - 9 * this->square_size) / 2 : 0)};
}

void Game_Destruct(Game *this) {
    TTF_CloseFont(this->font);
    TTF_Quit();
    SDL_DestroyTexture(this->pieces_texture);
    IMG_Quit();
    SDL_DestroyRenderer(this->renderer);
    SDL_DestroyWindow(this->window);
    SDL_Quit();
    Board_Destruct(&this->board);
}

static void Chess_DrawPiece(SDL_Renderer *renderer, SDL_Texture *pieces_texture, const Piece *piece, Position drawing_offset, int size) {
    static const int SIZE_ON_IMAGE = 52;
    const SDL_Rect src = {.x = (int)piece->type * 62 + 10, .y = (int)piece->color * 60 + 10, .w = SIZE_ON_IMAGE, .h = SIZE_ON_IMAGE};
    const SDL_Rect dest = {.x = drawing_offset.x, .y = drawing_offset.y, .w = size, .h = size};
    SDL_RenderCopy(renderer, pieces_texture, &src, &dest);
}

static void Chess_DrawSquareWithPiece(SDL_Renderer *renderer, SDL_Texture *pieces_texture, const Square *square, Position drawing_offset, int size) {
    SDL_Color color;
    if (Square_GetColor(square) == WHITE) {
        color = (SDL_Color){.r = 0xff, .g = 0xce, .b = 0x9e, .a = 0xff};
    } else {
        color = (SDL_Color){.r = 0xd1, .g = 0x8b, .b = 0x47, .a = 0xff};
    }
    boxRGBA(renderer, (Sint16)drawing_offset.x, (Sint16)drawing_offset.y, (Sint16)(drawing_offset.x + size - 1), (Sint16)(drawing_offset.y + size - 1), color.r, color.g, color.b, color.a);
    if (square->piece != NULL) {
        Chess_DrawPiece(renderer, pieces_texture, square->piece, drawing_offset, size);
    }
}

static void Chess_DrawTextToWhiteBox(SDL_Renderer *renderer, TTF_Font *font, const char *text, Position drawing_offset, Position size) {
    // Background
    boxRGBA(renderer, (Sint16)drawing_offset.x, (Sint16)drawing_offset.y, (Sint16)(drawing_offset.x + size.x - 1), (Sint16)(drawing_offset.y + size.y - 1),
            0xff, 0xff, 0xff, 0xff);
    // Text
    SDL_Rect dest = {.x = drawing_offset.x, .y = drawing_offset.y, .w = 0, .h = 0};
    int text_size;
    if (size.x <= size.y) {
        text_size = size.x;
        dest.y += text_size / 2;
    } else {
        text_size = size.y;
        dest.x += text_size / 2;
    }
    dest.w = text_size;
    dest.h = text_size;
    const SDL_Color black = {.r = 0x0, .g = 0x0, .b = 0x0, .a = 0xff};
    SDL_Surface *surface = TTF_RenderUTF8_Blended(font, text, black);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_RenderCopy(renderer, texture, NULL, &dest);
    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);
}

static void Game_Draw(const Game *this) {
    for (int y = 0; y < 8; ++y) {
        for (int x = 0; x < 8; ++x) {
            const Position drawing_offset = {.x = this->board_offset.x + this->edge_size + x * this->square_size,
                                             .y = this->board_offset.y + this->edge_size + y * this->square_size};
            Chess_DrawSquareWithPiece(this->renderer, this->pieces_texture, &this->board.squares[x][y], drawing_offset, this->square_size);
        }
    }
    Position size = {.x = this->square_size, .y = this->edge_size};
    for (int x = 0; x < 8; ++x) {
        const char text[2] = {(char)(x + 'A'), '\0'};
        Position drawing_offset = {.x = this->board_offset.x + this->edge_size + x * this->square_size, .y = this->board_offset.y};
        Chess_DrawTextToWhiteBox(this->renderer, this->font, text, drawing_offset, size);
        drawing_offset.y = this->board_offset.y + this->edge_size + 8 * this->square_size;
        Chess_DrawTextToWhiteBox(this->renderer, this->font, text, drawing_offset, size);
    }
    size = (Position){.x = this->edge_size, .y = this->square_size};
    for (int y = 0; y < 8; ++y) {
        const char text[2] = {(char)('8' - y), '\0'};
        Position drawing_offset = {.x = this->board_offset.x, .y = this->board_offset.y + this->edge_size + y * this->square_size};
        Chess_DrawTextToWhiteBox(this->renderer, this->font, text, drawing_offset, size);
        drawing_offset.x = this->board_offset.x + this->edge_size + 8 * this->square_size;
        Chess_DrawTextToWhiteBox(this->renderer, this->font, text, drawing_offset, size);
    }
    SDL_RenderPresent(this->renderer);
}

void Game_Run(Game *this) {
    Game_Draw(this);
    bool gameIsRunning = true;
    while (gameIsRunning) {
        SDL_Event event;
        while (SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                case SDL_QUIT:
                    gameIsRunning = false;
                    break;
                default:
                    break;
            }
        }
        Game_Draw(this);
    }
}
