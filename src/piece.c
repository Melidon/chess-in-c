#include <stdio.h>
#include <stdlib.h>

#include "piece.h"

void Piece_Construct(Piece *this, Color color, PieceType type) {
    *this = (Piece){.color = color, .type = type};
}

Piece *Piece_New(Color color, PieceType type) {
    Piece *this = malloc(sizeof(Piece));
    if (this == NULL) {
        perror("Piece_New");
        exit(EXIT_FAILURE);
    }
    Piece_Construct(this, color, type);
    return this;
}

void Piece_Destruct(Piece *this) {
}

void Piece_Delete(Piece *this) {
    Piece_Destruct(this);
    free(this);
}
