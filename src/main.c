#include "game.h"
#include <stdlib.h>

int main(void) {
    Game game;
    Game_Construct(&game, 468, 468);
    Game_Run(&game);
    Game_Destruct(&game);
    return EXIT_SUCCESS;
}
