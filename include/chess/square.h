#ifndef SQUARE_H_INCLUDED
#define SQUARE_H_INCLUDED

#include "piece.h"
#include "position.h"

/**
 * @brief Represents a square on a chess board.
 */
typedef struct Square {
    Position position;
    Piece *piece;
} Square;

/**
 * @brief Constructs a Square object with the given x and y coordinates.
 *
 * @param this A pointer to the Square object to construct.
 * @param x The x coordinate of the Square.
 * @param y The y coordinate of the Square.
 */
void Square_Construct(Square *this, int x, int y);

/**
 * @brief Destructs a Square object and frees its memory.
 *
 * @param this Pointer to the Square object to be destructed.
 */
void Square_Destruct(Square *this);

/**
 * @brief Sets the piece on the square.
 *
 * @param this The square to set the piece on.
 * @param piece The piece to set on the square.
 */
void Square_SetPiece(Square *this, Piece *piece);

/**
 * @brief Gets the color of the square.
 *
 * @param this The square to get the color from.
 * @return The color of the square.
 */
Color Square_GetColor(const Square *this);

#endif // SQUARE_H_INCLUDED
