#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

/**
 * @brief Represents the color of a chess piece.
 */
typedef enum Color {
    WHITE = 0,
    BLACK = 1
} Color;

#endif // COLOR_H_INCLUDED
