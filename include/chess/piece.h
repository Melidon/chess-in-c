#ifndef PIECE_H_INCLUDED
#define PIECE_H_INCLUDED

#include "color.h"

/**
 * @brief Enum representing the type of a chess piece.
 */
typedef enum PieceType {
    KING = 0,
    QUEEN = 1,
    ROOK = 2,
    BISHOP = 3,
    KNIGHT = 4,
    PAWN = 5
} PieceType;

/**
 * @brief Represents a chess piece, consisting of a color and a type.
 */
typedef struct Piece {
    Color color;
    PieceType type;
} Piece;

/**
 * @brief Constructs a Piece object with the specified color and type.
 *
 * @param this Pointer to the Piece object to construct.
 * @param color The color of the Piece object.
 * @param type The type of the Piece object.
 */
void Piece_Construct(Piece *this, Color color, PieceType type);

/**
 * @brief Creates a new Piece object with the specified color and type.
 *
 * @param color The color of the piece.
 * @param type The type of the piece.
 * @return A pointer to the newly created Piece object.
 */
Piece *Piece_New(Color color, PieceType type);

/**
 * @brief Destructs a Piece object.
 *
 * @param this Pointer to the Piece object to be destructed.
 */
void Piece_Destruct(Piece *this);

/**
 * @brief Deletes a Piece object and frees its memory.
 *
 * @param this A pointer to the Piece object to be deleted.
 */
void Piece_Delete(Piece *this);

#endif // PIECE_H_INCLUDED
