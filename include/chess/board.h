#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED

#include "square.h"

/**
 * @brief Represents a chess board.
 */
typedef struct Board {
    Square squares[8][8];
} Board;

/**
 * @brief Constructs a Board object.
 * 
 * @param this Pointer to the Board object to construct.
 */
void Board_Construct(Board *this);

/**
 * @brief Destructs a Board object.
 *
 * @param this Pointer to the Board object to be destructed.
 */
void Board_Destruct(Board *this);

#endif // BOARD_H_INCLUDED
