#ifndef POSITION_H_INCLUDED
#define POSITION_H_INCLUDED

/**
 * @brief Represents a position on a chess board.
 */
typedef struct Position {
    int x;
    int y;
} Position;

#endif // POSITION_H_INCLUDED
