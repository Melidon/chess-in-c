#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "board.h"

/**
 * @brief Represents a game of chess.
 *
 * This struct contains the board, window, renderer, pieces texture, font, square size, edge size, and board offset.
 */
typedef struct Game {
    Board board;
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Texture *pieces_texture;
    TTF_Font *font;
    int square_size;
    int edge_size;
    Position board_offset;
} Game;

/**
 * @brief Constructs a new Game object with the specified width and height.
 *
 * @param this Pointer to the Game object to construct.
 * @param width The width of the game board.
 * @param height The height of the game board.
 */
void Game_Construct(Game *this, int width, int height);

/**
 * @brief Destructs a Game object.
 *
 * @param this A pointer to the Game object to be destructed.
 */
void Game_Destruct(Game *this);

/**
 * @brief  Runs the game.
 *
 * This function starts and runs the game until it is over.
 *
 * @param this A pointer to the Game object.
 */
void Game_Run(Game *this);

#endif // GAME_H_INCLUDED
